[general]
name=French Locator Filter
category=Filter

about=The geocoding API is an open service provided and maintained by the French Government.
about[fr]=L'API du gouvernement français est un service de geocodage gratuit et ouvert
    base sur la Base Adresse Nationale (BAN). Ce plugin ajoute un filtre de recherche
    basée sur ce service dans la barre de recherche universelle (Localisateur).

description=Adds a geocoder to the QGIS Locator Filter, using the national French open address API.
description[fr]=Ajoute un filtre de géocodage basé sur l'API de la BAN dans le
    Localisateur (barre de recherche universelle) de QGIS.

icon=resources/images/icon.svg

tags=geocoder, locator, filter, addok, France, french
tags[fr]=geocodeur, recherche, adresse, France, ADDOK, Etalab, BAN, BANO

# credits and contact
author=Régis Haubourg (Oslandia), Richard Duivenvoorde, Julien Moura (Oslandia)
email=qgis@oslandia.com
homepage=https://oslandia.gitlab.io/qgis/french_locator_filter/
repository=https://gitlab.com/Oslandia/qgis/french_locator_filter/
tracker=https://gitlab.com/Oslandia/qgis/french_locator_filter/-/issues

# status
deprecated=False
experimental=False
qgisMinimumVersion=3.16
qgisMaximumVersion=3.99

# versioning
version=1.1.1
changelog=

[oslandia]
funders=ANFSI,Bordeaux Métropole,Grand Lyon,Orange
screenshot=docs/_static/images/french_locator_sample_basque_qgis_details.png
issue_to_be_funded=https://gitlab.com/Oslandia/qgis/french_locator_filter/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=To%20be%20funded&first_page_size=100
blog_link=https://oslandia.com/2024/05/31/plugin-qgis-french-locator-filter-1-1-0-api-photon/
